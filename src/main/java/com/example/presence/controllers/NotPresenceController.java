package com.example.presence.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.views.NotPresenceDetail;
import com.io.iona.springboot.controllers.HibernateViewController;

@CrossOrigin(allowCredentials = "true")
@RestController
@RequestMapping("/api/notPresence")
public class NotPresenceController extends HibernateViewController<NotPresenceDetail, NotPresenceDetail>{
	
}
