package com.example.presence.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.dtomodels.EmployeeDTO;
import com.example.presence.models.Employee;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController extends HibernateCRUDController<Employee, EmployeeDTO> {

}
