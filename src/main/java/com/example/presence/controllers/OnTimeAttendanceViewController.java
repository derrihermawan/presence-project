package com.example.presence.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.views.OnTimeAttendance;
import com.io.iona.springboot.controllers.HibernateViewController;


@CrossOrigin(allowCredentials = "true")
@RestController
@RequestMapping("/api/attendance-ontime")
public class OnTimeAttendanceViewController extends HibernateViewController<OnTimeAttendance, OnTimeAttendance>{
	
}
