package com.example.presence.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.dtomodels.ReligionDTO;
import com.example.presence.models.Religion;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/religion")
public class ReligionController extends HibernateCRUDController<Religion, ReligionDTO> {
	
}
