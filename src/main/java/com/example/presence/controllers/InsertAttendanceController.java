package com.example.presence.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.config.Konfigurasi;
import com.example.presence.dtomodels.AttendanceDTO;
import com.example.presence.dtomodels.EmployeeDTO;
import com.example.presence.dtomodels.StagingAttendanceDTO;
import com.example.presence.models.Attendance;
import com.example.presence.models.Employee;
import com.example.presence.models.StagingAttendance;
import com.example.presence.repositories.AttendanceRepository;
import com.example.presence.repositories.EmployeeRepository;
import com.example.presence.repositories.LogRepository;
import com.example.presence.repositories.StagingAttendanceRepository;

@RestController
@RequestMapping("/api/insertAttendance")
public class InsertAttendanceController {

	@Autowired 
	StagingAttendanceRepository stagingAttendanceRepository;
	
	@Autowired 
	AttendanceRepository attendanceRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	LogRepository logRepository;
		
	ModelMapper modelMapper = new ModelMapper();
	
	private List<StagingAttendanceDTO> mappingStagingAttendance(){
		List<StagingAttendance> listStagingAttendance = stagingAttendanceRepository.findAll();
		List<StagingAttendanceDTO> listStagingAttendanceDTO = new ArrayList<StagingAttendanceDTO>();
		for(StagingAttendance stagingAttendance : listStagingAttendance) {
			StagingAttendanceDTO stagingAttendanceDTO = modelMapper.map(stagingAttendance, StagingAttendanceDTO.class);
			listStagingAttendanceDTO.add(stagingAttendanceDTO);
		}
		return listStagingAttendanceDTO;
	}
		
	private List<AttendanceDTO> mappingAttendance(){
		List<Attendance> listAttendance = attendanceRepository.findAll();
		List<AttendanceDTO> listAttendanceDTO = new ArrayList<AttendanceDTO>();
		for(Attendance attendance : listAttendance) {
			AttendanceDTO attendanceDTO = modelMapper.map(attendance, AttendanceDTO.class);
			listAttendanceDTO.add(attendanceDTO);
		}
		return listAttendanceDTO;
	}
	
	private List<EmployeeDTO> mappingEmployee(){
		List<Employee> listEmployee = employeeRepository.findAll();
		List<EmployeeDTO> listEmployeeDTO = new ArrayList<EmployeeDTO>();
		for(Employee employee : listEmployee) {
			EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
			listEmployeeDTO.add(employeeDTO);
		}
		return listEmployeeDTO;
	}
	
	@PostMapping("/insertData")
	public String save() throws Exception{
		Konfigurasi konfigurasi = new Konfigurasi();
		JSONArray request = konfigurasi.konfigur();
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatterTime = new SimpleDateFormat("H:mm:ss:SS");
		SimpleDateFormat formatterTime1 = new SimpleDateFormat("H.mm.ss");
		Date timeInMin = formatterTime.parse(request.getJSONObject(0).getString("timeInMin"));
		int timeInMinHour = timeInMin.getHours();
		int timeInMinMinute = timeInMin.getMinutes();
		int timeInMinSecond = timeInMin.getSeconds();
		Date timeInMax = formatterTime.parse(request.getJSONObject(0).getString("timeInMax"));
		int timeInMaxHour = timeInMax.getHours();
		int timeInMaxMinute = timeInMax.getMinutes();
		int timeInMaxSecond = timeInMax.getSeconds();
		Date timeOutMin = formatterTime.parse(request.getJSONObject(0).getString("timeOutMin"));
		int timeOutMinHour = timeOutMin.getHours();
		int timeOutMinMinute = timeOutMin.getMinutes();
		int timeOutMinSecond = timeOutMin.getSeconds();
		Date timeOutMax = formatterTime.parse(request.getJSONObject(0).getString("timeOutMax"));
		int timeOutMaxHour = timeOutMax.getHours();
		int timeOutMaxMinute = timeOutMax.getMinutes();
		int timeOutMaxSecond = timeOutMax.getSeconds();
		Long statusProject = request.getJSONObject(0).getLong("status");
		Date defaultDuration = formatterTime.parse(request.getJSONObject(0).getString("defaultDuration"));
		
		String result = null;
		List<StagingAttendanceDTO> listStagingAttendanceDTO = mappingStagingAttendance();
		List<EmployeeDTO> listEmployeeDTO = mappingEmployee();
				
		for(StagingAttendanceDTO item : listStagingAttendanceDTO){
			String dateTime = item.getDateTime();
			Date dateItem = formatter.parse(dateTime);
			int year = dateItem.getYear();
			int month = dateItem.getMonth();
			int day = dateItem.getDate();
			int hour = dateItem.getHours();
			int minute = dateItem.getMinutes();
			int second = dateItem.getSeconds();
			
			Long nip = item.getPin();
			String tempMonth = String.valueOf(month+1);
			if(month<10) {
				tempMonth = "0" +(month+1);
			}
			String tempDate = (1900+year)+"-"+tempMonth+"-"+day;
			Date date = formatterDate.parse(tempDate);
			Date time = formatterTime.parse(hour+":"+minute+":"+second+":00");
			Date timeIn = defaultDuration;
			Date timeOut = defaultDuration;
			if(hour >= timeInMinHour && hour <= timeInMaxHour) {
				timeIn = time;
			}
			else if(hour >= timeOutMinHour && hour <= timeOutMaxHour) {
				timeOut = time;
			}

			List<AttendanceDTO> listAttendanceDTO = mappingAttendance();
			Boolean add = true;
			Boolean update = false;
			if(listAttendanceDTO.size()>=1) {
				for(AttendanceDTO itemAttendance : listAttendanceDTO) {
					Date dateAttendance = itemAttendance.getDate();
					Long nipItemAttendance = itemAttendance.getEmployee().getNip();
					if(date.getTime()==dateAttendance.getTime() && nip.equals(nipItemAttendance)){
						add = false;
						Date timeInItemAttendance = itemAttendance.getTimeIn();
						if(timeInItemAttendance!=null && !(timeOut.equals(defaultDuration))) {
							update = true;
							nip = nipItemAttendance;
							timeIn = timeInItemAttendance;
						}
					}
				}
			}

			Long status = (long) 0;
			for(EmployeeDTO itemEmployee : listEmployeeDTO) {
				Long nipEmployee = itemEmployee.getNip();
				if(nipEmployee.equals(nip)) {
					status = itemEmployee.getStatus().getStatusId();
				}
			}
			
			Date dateDefault = null;
			int hourLimit = 0;
			int minuteLimit = 0;
			int secondLimit = 0;
			if(status.equals(statusProject)) {
				dateDefault = formatterTime.parse(request.getJSONObject(0).getString("limitTimeInForProject"));
				hourLimit = dateDefault.getHours();
				minuteLimit = dateDefault.getMinutes();
				secondLimit = dateDefault.getSeconds();
			}
			else {
				dateDefault = formatterTime.parse(request.getJSONObject(0).getString("limitTimeInForOther"));
				hourLimit = dateDefault.getHours();
				minuteLimit = dateDefault.getMinutes();
				secondLimit = dateDefault.getSeconds();
			}
			Date firstTime = null;
			Date secondTime = null;
			Date lateTimeDuration = null;
			Date earlyInDuration = null;
			if(hour >= hourLimit && minute >= minuteLimit && second > secondLimit) {
				firstTime = formatterTime1.parse(hourLimit+"."+minuteLimit+"."+secondLimit);
				secondTime = formatterTime1.parse(hour+"."+minute+"."+second);
				lateTimeDuration = countRange(firstTime, secondTime);
				earlyInDuration = defaultDuration;
			}
			else if(hour == hourLimit && minute == minuteLimit && second == secondLimit) {
				lateTimeDuration = defaultDuration;
				earlyInDuration = defaultDuration;
			}
			else {
				firstTime = formatterTime1.parse(hour+"."+minute+"."+second);
				secondTime = formatterTime1.parse(hourLimit+"."+minuteLimit+"."+secondLimit);
				earlyInDuration = countRange(firstTime, secondTime);
				lateTimeDuration = defaultDuration;
			}

			Date workingTimeDuration = defaultDuration;
			if(add) {
				try {
					attendanceRepository.insert(nip, date, timeIn, timeOut, lateTimeDuration, earlyInDuration, workingTimeDuration);
					result = "Raspberry: Berhasil menyimpan data absensi.";
					
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data absensi. " + e.getMessage();
		        	logRepository.insertLog(result);
				}
			}
			
			if(update) {
				if(timeIn.equals(defaultDuration)) {
					timeIn = dateDefault;
				}
				workingTimeDuration = countRange(timeIn, timeOut);
				try {
					attendanceRepository.update(nip, date, timeOut, workingTimeDuration);
					result = "Raspberry: Berhasil mengupdate data absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal mengupdate data absensi. " + e.getMessage();
		        	logRepository.insertLog(result);
				}
			}
		}		
		return result;
	}
	
	private Date countRange(Date firstTime, Date secondTime) throws ParseException {
	    Date result = null;
        int firstHour = firstTime.getHours();
        int secondHour = secondTime.getHours();
        int firstMinute = firstTime.getMinutes();
        int secondMinute = secondTime.getMinutes();
        int firstSecond = firstTime.getSeconds();
        int secondSecond = secondTime.getSeconds();
             
        int secondRange = 0;
        if(firstSecond > secondSecond) {
            while(firstSecond != secondSecond) {
                if(firstSecond == 60) {
                	firstSecond = 0;
                    firstMinute++;
                        continue;
                }
                firstSecond++;
                secondRange++;
            }
        }
        else if(firstSecond < secondSecond) {
        	secondRange = secondSecond - firstSecond;
        }
        int timeRange = secondRange;    
        int minuteRange = 0;
         
        if(firstMinute > secondMinute) {
            while(firstMinute != secondMinute) {
                if(firstMinute == 60) {
                	firstMinute = 0;
                    firstHour++;
                        continue;
                }
                firstMinute++;
                minuteRange++;
            }
        }
        else if(firstMinute < secondMinute) {
        	minuteRange = secondMinute - firstMinute;          
        }
         
        timeRange += minuteRange * 60;
        int hourRange = 0;
        if(firstHour > secondHour) {
            while(firstHour != secondHour) {
                if(firstHour == 24) {                
                	firstHour = 0;
                    continue;
                }
                firstHour++;
                hourRange++;
            }
        }
        else if(firstHour < secondHour) {
        	hourRange = secondHour - firstHour;
        }
        timeRange += hourRange * 3600;
        int hour = timeRange / 3600;
        timeRange %= 3600;
         
        int minute = 0;
        int second = 0;
        if(timeRange >= 60) {
        	minute = timeRange / 60;
        	second = timeRange % 60;
             
        } else {
        	minute = 0;
        	second = timeRange;
        }
        
        SimpleDateFormat df = new SimpleDateFormat("H.mm.ss");
        result = df.parse(hour+"."+minute+"."+second);
        return result;
	}
}
