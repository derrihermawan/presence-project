package com.example.presence.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.penarikan.PenarikanAttendance;
import com.example.presence.repositories.StagingAttendanceRepository;

@RestController
@RequestMapping("/api/getAttendance")
public class GetAttendanceController {
	@Autowired
	StagingAttendanceRepository stagingAttendanceRepository;
	
	ModelMapper modelMapper = new ModelMapper();
		
	@PostMapping("/getData")
	public String save() throws Exception{
		PenarikanAttendance penarikan = new PenarikanAttendance();
		
		String data = penarikan.getDataFromMachine(); 
		String result = null;
		
		String newData = data.substring(0, data.length()-1);
		for (String rows: newData.split("\n"))
		{
			int j = 1;
			String dateTime = null;
			
			Long pin = null;
			String verified = null;
			String status = null;
			String workCode = null;
				
			for (String fields: rows.split("_"))
			{
				switch(j)
				{
					case 1:
						pin = Long.parseLong(fields);
						break;
					case 2:				
						dateTime = fields;	
						break;
					case 3:				
						verified = fields;
						break;
					case 4:
						status = fields;
						break;	
					case 5:
						workCode = fields;
						break;
					default:
						break;
				}
				j++;
			}

			boolean add = false;
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = formatter.parse(dateTime);
			Date currentDate = new Date();
			int year = date.getYear();
			int month = date.getMonth();
			int day = date.getDate();
			int currentYear = currentDate.getYear();
			int currentMonth = currentDate.getMonth();
			int currentDay = currentDate.getDate();
			if(year==currentYear && month==currentMonth && day==currentDay) {
				add = true;
			}
			
			if(add) {
				try {
					stagingAttendanceRepository.insert(pin, dateTime, verified, status, workCode);
					result = "Raspberry: Berhasil menyimpan data staging absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data staging absensi. " + e.getMessage();
				}
			}
		}		
		return result;
	}
}
