package com.example.presence.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.repositories.StagingAttendanceRepository;

@RestController
@RequestMapping("/api/deleteStaging")
public class DeleteStagingController {
	@Autowired
	StagingAttendanceRepository stagingAttendanceRepository;
	
	@DeleteMapping("/deleteData")
	public String deleteStaging() { 
		String result;
		try {
			stagingAttendanceRepository.delete();
			result = "Raspberry: Berhasil menghapus data staging absensi.";
		}catch (Exception e) {
        	result = "Raspberry: Gagal menghapus data staging absensi. " + e.getMessage();
		}
		return result;
	}
}
