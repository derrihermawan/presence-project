package com.example.presence.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.views.Report;
import com.io.iona.springboot.controllers.HibernateViewController;

@CrossOrigin(allowCredentials = "true")
@RestController
@RequestMapping("/api/report")
public class ReportController extends HibernateViewController<Report, Report>{

}
