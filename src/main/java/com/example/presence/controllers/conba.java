package com.example.presence.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.dtomodels.AttendanceDTO;
import com.example.presence.dtomodels.EmployeeDTO;
import com.example.presence.dtomodels.StagingAttendanceDTO;
import com.example.presence.models.Attendance;
import com.example.presence.models.Employee;
import com.example.presence.models.StagingAttendance;
import com.example.presence.repositories.AttendanceRepository;
import com.example.presence.repositories.EmployeeRepository;
import com.example.presence.repositories.LogRepository;
import com.example.presence.repositories.StagingAttendanceRepository;

@RestController
@RequestMapping("/api/coba")
public class conba {
	@Autowired 
	EmployeeRepository employeeRepository;

	@Autowired 
	StagingAttendanceRepository stagingAttendanceRepository;
	
	@Autowired 
	AttendanceRepository attendanceRepository;
	
	@Autowired
	LogRepository logRepository;
		
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/get/{numberOfData}/{page}")
	public Map<String, Object> lateAll(  
			//@PathVariable(value="sortField") String sortField, @PathVariable(value="sortMethod") String sortMethod, 
			@PathVariable(value="numberOfData") int numberOfData, @PathVariable(value="page") int page)throws ParseException, JSONException{
//			@PathVariable(value="startDate") String startDateString, @PathVariable(value="endDate") String endDateString) throws ParseException, JSONException{
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> listMap = new HashMap<String, Object>();
		List<Map<String, Object>> listResult = new ArrayList<Map<String,Object>>();
		List<AttendanceDTO> listAttendanceDTO = new ArrayList<AttendanceDTO>();
		List<EmployeeDTO> listEmployeeDTO = mappingEmployee();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Pageable pageable = null; 
//		if(sortMethod.equalsIgnoreCase("descending")) {
//			pageable = PageRequest.of(page, numberOfData, Sort.by(sortField).descending());
//		}
//		else if(sortMethod.equalsIgnoreCase("ascending")) {
//			pageable = PageRequest.of(page, numberOfData, Sort.by(sortField).ascending());
//		}
//		else {
			pageable = PageRequest.of(page, numberOfData);
//		}

		//Page<Attendance> listAttendance = attendanceRepository.findAllNotPresence(pageable);
		List<Attendance> listAttendance = attendanceRepository.findAll();
//		if(startDateString!=null && endDateString!=null) {
//			Date startDate = df.parse(startDateString);
//			Date endDate = df.parse(endDateString);
//			listAttendance = attendanceRepository.findAllNotPresenceByDate(startDate, endDate, pageable);
//		}
		for(Attendance item : listAttendance) {
			AttendanceDTO data = modelMapper.map(item, AttendanceDTO.class);
			listAttendanceDTO.add(data);
		}
		
		List<Date> listDate = new ArrayList<Date>();
		for(AttendanceDTO itemAtt : listAttendanceDTO) {
			Date date = itemAtt.getDate();
			Boolean add = true;
			if(listDate.size()>=1) {
				for(Object itemDate : listDate) {
					if(date.equals(itemDate)) {
						add = false;
					}
				}
			}
			if(add) {
				listDate.add(date);
			}
		}
		int i=0;
		for(EmployeeDTO itemEmployee : listEmployeeDTO) {
			Long nipEmployee = itemEmployee.getNip();
			String name = itemEmployee.getName();
			for(Date itemDate : listDate) {
				Date tempDate = itemDate;
				Boolean notExists = true;
				for(AttendanceDTO itemAtt : listAttendanceDTO) {
					Date dateAtt = itemAtt.getDate();
					Long nipAtt = itemAtt.getEmployee().getNip();
					if(nipEmployee.equals(nipAtt) && tempDate.equals(dateAtt)) {
						notExists = false;
					}
				}
				if(notExists) {
					listMap.put("nip", nipEmployee);
					listMap.put("name", name);
					listMap.put("date", tempDate);
					Boolean add = true;
					if(listResult.size()>=1) {
						for(Map<String, Object> map : listResult) {
							if(nipEmployee.equals(map.get("nip")) && tempDate.equals(map.get("date"))) {
								add = false;
							}
						}
					}
					System.out.println("add "+ add);
					if(add) {
						listResult.add(listMap);
					}
				}
				i++;
			}
		}
		result.put("Status", 200);
		result.put("Message", "Get List SUCCESSFULL" );
		result.put("Data", listResult);
		System.out.println("list "+ listResult);
		return result;
	}
	
	//mapping employee
	private List<EmployeeDTO> mappingEmployee(){
		List<Employee> listEmployee = employeeRepository.findAll();
		List<EmployeeDTO> listEmployeeDTO = new ArrayList<EmployeeDTO>();
		for(Employee employee : listEmployee) {
			EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
			listEmployeeDTO.add(employeeDTO);
		}
		return listEmployeeDTO;
	}	

	@GetMapping("/date")
	private Date countRange() throws ParseException {
	    Date result = null;
        //  Tampilkan Waktu Awal dan Waktu Akhir
        String str_waktu_awal = "07.38.40";
        String str_waktu_akhir = "08.00.00";
        SimpleDateFormat df = new SimpleDateFormat("H.mm.ss");
        Date awal = df.parse(str_waktu_awal);
        Date akhir = df.parse(str_waktu_akhir);
        //  Konversi variable str menjadi integer
        int jam_awal = awal.getHours();
        int jam_akhir = akhir.getHours();
        int menit_awal = awal.getMinutes();
        int menit_akhir = akhir.getMinutes();
        int detik_awal = awal.getSeconds();
        int detik_akhir = akhir.getSeconds();
         
        //  Proses pencarian selisih Waktu Awal dan Waktu Akhir
        //  Langkah Ketiga, cari selisih detik awal dan detik akhir     
        int selisih_detik = 0;
         
        //  Jika detik awal > detik akhir
        //  Contohnya, detik awal = 40 dan detik akhir = 30
        if(detik_awal > detik_akhir) {
            while(detik_awal != detik_akhir) {
                if(detik_awal == 60) {
                    detik_awal = 0;
                    menit_awal++;
                        continue;
                }
                detik_awal++;
                selisih_detik++;
            }
        }
         
        //  Jika detik awal < detik akhir
        //  Contohnya, detik awal = 30 dan detik akhir = 40
        else if(detik_awal < detik_akhir) {
            selisih_detik = detik_akhir - detik_awal;
        }
         
        //  Masukkan ke variable selisih waktu
        int selisih_waktu = selisih_detik;
         
        //-------------------------------------------------------
        //  Langkah Kedua, cari selisih menit awal dan menit akhir      
        int selisih_menit = 0;
         
        //  Jika menit awal > menit akhir.
        //  Contohnya, menit awal = 50 dan menit akhir = 10
        if(menit_awal > menit_akhir) {
            while(menit_awal != menit_akhir) {
                if(menit_awal == 60) {
                    menit_awal = 0;
                    jam_awal++;
                        continue;
                }
                menit_awal++;
                selisih_menit++;
            }
        }
         
        //  Jika menit awal < menit akhir
        //  Contohnya, menit awal = 10 dan menit akhir = 50
        else if(menit_awal < menit_akhir) {
            selisih_menit = menit_akhir - menit_akhir;          
        }
         
        //  Ubah selisih menit menjadi satuan detik (1 menit = 60 detik)
        selisih_waktu += selisih_menit * 60;
         
        //  Langkah Pertama, cari selisih jam awal dan jam akhir        
        int selisih_jam = 0;
         
        //  Jika jam awal > jam akhir. Contohnya, Jam Awal = 22 (10 malam) dan Jam Akhir = 05 (5 pagi)
        if(jam_awal > jam_akhir) {
            //  Cocokkan antara jam awal dan jam akhir
            while(jam_awal != jam_akhir) {
                if(jam_awal == 24) {                
                    jam_awal = 0;
                    continue;
                }
                jam_awal++;
                selisih_jam++;
            }
        }
         
        //  Jika jam awal < jam akhir. Contohnya, Jam Awal = 05 (5 pagi) dan Jam Akhir = 22 (10 malam)
        else if(jam_awal < jam_akhir) {
            selisih_jam = jam_akhir - jam_awal;
        }
         
        //  Ubah selisih jam menjadi satuan detik (1 jam = 3600 detik)
        selisih_waktu += selisih_jam * 3600;
         
        //  Ubah selisih_waktu menjadi format waktu HH:mm:ss
        //  Ubah selisih waktu menjadi satuan jam
        int jam = selisih_waktu / 3600;
        selisih_waktu %= 3600;
         
        //  Ubah selisih waktu menjadi satuan menit
        int menit = 0;
        int detik = 0;
        if(selisih_waktu >= 60) {
            menit = selisih_waktu / 60;
            detik = selisih_waktu % 60;
             
        } else {
            menit = 0;
            detik = selisih_waktu;
        }
        result = df.parse(jam+"."+menit+"."+detik);
        return result;
	}
}
