package com.example.presence.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.dtomodels.StatusDTO;
import com.example.presence.models.Status;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/status")
public class StatusController extends HibernateCRUDController<Status, StatusDTO> {
	
}
