package com.example.presence.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.dtomodels.AttendanceDTO;
import com.example.presence.models.Attendance;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/attendance")
public class AttendanceController extends HibernateCRUDController<Attendance, AttendanceDTO>{

}
