package com.example.presence.views;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="vw_late", schema="public")
public class LateDetail {
	private Integer rowNumber;
	private Long nip;
	private String name;
	private Date date;
	private Date timeIn;
	private Date lateTimeDuration;
	
	@Id
	@Column(name = "no",  unique = true, nullable = false)
	public Integer getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}
	
	@Column(name = "nip", nullable = false)
	public Long getNip() {
		return nip;
	}
	public void setNip(Long nip) {
		this.nip = nip;
	}
	
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
    @Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = false)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

    @Temporal(TemporalType.TIME)
	@Column(name = "time_in", nullable = false)
	public Date getTimeIn() {
		return timeIn;
	}
	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}

    @Temporal(TemporalType.TIME)
	@Column(name = "late_time_duration", nullable = false)
	public Date getLateTimeDuration() {
		return lateTimeDuration;
	}
	public void setLateTimeDuration(Date lateTimeDuration) {
		this.lateTimeDuration = lateTimeDuration;
	}
}
