package com.example.presence.views;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="view_ontime", schema="public")
public class OnTimeAttendance {

	private Integer rowNumber;
	private Long nip;
	private String name;
	private Date date;
	private Date timeIn;
	private Date earlyInDuration;

	@Id
	@Column(name = "no",  unique = true, nullable = false)
	public Integer getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}
	
	@Column(name = "nip", nullable = false)
	public Long getNip() {
		return nip;
	}
	public void setNip(Long nip) {
		this.nip = nip;
	}

    @Column(name = "name", nullable = true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = true)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "time_in", length = 15)
	public Date getTimeIn() {
		return timeIn;
	}
	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "early_in_duration", nullable = true)
	public Date getEarlyInDuration() {
		return earlyInDuration;
	}
	public void setEarlyInDuration(Date earlyInDuration) {
		this.earlyInDuration = earlyInDuration;
	}
}
