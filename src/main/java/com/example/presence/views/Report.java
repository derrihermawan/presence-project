package com.example.presence.views;


import java.time.Duration;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.print.attribute.standard.MediaSize.Other;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.interval.PostgreSQLIntervalType;

@Entity
@Immutable
@Table(name="vw_report", schema="public")
@TypeDef(
	    typeClass = PostgreSQLIntervalType.class,
	    defaultForType = Duration.class
	)
public class Report {
	private Integer rowNumber;
	private Long nip;
	private String name;
	private Long jumlahHariEfektif;
	private Long jumlahMasuk;
	private Long jumlahKesiangan;
	private Long jumlahTidakHadir;
	private Duration totalJamKerja;
	private Duration totalJamKesiangan;
	@Id
	@Column(name = "no",  unique = true, nullable = false)
	public Integer getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}
	@Column(name = "nip", nullable = false)
	public Long getNip() {
		return nip;
	}
	public void setNip(Long nip) {
		this.nip = nip;
	}
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "jumlah_hari_efektif", nullable = false)
	public Long getJumlahHariEfektif() {
		return jumlahHariEfektif;
	}
	public void setJumlahHariEfektif(Long jumlahHariEfektif) {
		this.jumlahHariEfektif = jumlahHariEfektif;
	}
	@Column(name = "jumlah_masuk", nullable = false)
	public Long getJumlahMasuk() {
		return jumlahMasuk;
	}
	public void setJumlahMasuk(Long jumlahMasuk) {
		this.jumlahMasuk = jumlahMasuk;
	}
	@Column(name = "jumlah_kesiangan", nullable = false)
	public Long getJumlahKesiangan() {
		return jumlahKesiangan;
	}
	public void setJumlahKesiangan(Long jumlahKesiangan) {
		this.jumlahKesiangan = jumlahKesiangan;
	}
	@Column(name = "jumlah_tidak_hadir", nullable = false)
	public Long getJumlahTidakHadir() {
		return jumlahTidakHadir;
	}
	public void setJumlahTidakHadir(Long jumlahTidakHadir) {
		this.jumlahTidakHadir = jumlahTidakHadir;
	}
	
	@Column(name = "total_jam_kerja", nullable = false,
	        columnDefinition = "interval")
	public Duration getTotalJamKerja() {
		return totalJamKerja;
	}
	public void setTotalJamKerja(Duration totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}
	
	@Column(name = "total_jam_kesiangan", nullable = false,
	        columnDefinition = "interval")
	public Duration getTotalJamKesiangan() {
		return totalJamKesiangan;
	}
	public void setTotalJamKesiangan(Duration totalJamKesiangan) {
		this.totalJamKesiangan = totalJamKesiangan;
	}
	

}
