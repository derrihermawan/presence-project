package com.example.presence;



import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.presence.trigger.AttendancePenarikanJobTrigger;

@SpringBootApplication
public class PresenceApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(PresenceApplication.class, args);
		AttendancePenarikanJobTrigger attFPM = new AttendancePenarikanJobTrigger();
    	JobDetail getAttJobDetail = attFPM.getJobDetail();
    	Trigger getAttJobTrigger = attFPM.getTrigger();
    	
    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
//		scheduler.start();
//		scheduler.scheduleJob(getAttJobDetail, getAttJobTrigger);
	}

}
