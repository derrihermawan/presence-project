package com.example.presence.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;

public class Konfigurasi {

	public JSONArray konfigur() throws JSONException {
		// TODO Auto-generated method stub
		BufferedReader br = null;
		JSONArray data = null;
		try {

			String sCurrentLine;
			String buffer = "";
			br = new BufferedReader(new FileReader("konfigurasi.txt"));
			while ((sCurrentLine = br.readLine()) != null) {
				buffer = buffer + sCurrentLine;
				//System.out.println(buffer);
			}
			
			data = new JSONArray(buffer);            
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return data;
	}
}
