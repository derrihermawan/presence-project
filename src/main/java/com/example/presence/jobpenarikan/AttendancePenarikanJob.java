package com.example.presence.jobpenarikan;

import java.net.HttpURLConnection;
import java.net.URL;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.web.bind.annotation.RestController;

import com.example.presence.controllers.AttendanceController;


@RestController
public class AttendancePenarikanJob implements Job {
	AttendanceController attController = new AttendanceController();
	
	/*
	 * Method name	: execute
	 * Method type	: void
	 * Description	: This method implement job execution of GetSaveAttendance class
	 * Return value	: -
	 */	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			final String uri = "http://localhost:8080/api/stagingAttendance/insert";
		    HttpURLConnection httpClient = (HttpURLConnection) new URL(uri).openConnection(); 
		    httpClient.setRequestMethod("POST");
	        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient.setDoOutput(true);
	        httpClient.getResponseCode();
	        httpClient.connect();
			
			final String uri1 = "http://localhost:8080/api/attendance/insert";
		    HttpURLConnection httpClient1 = (HttpURLConnection) new URL(uri1).openConnection();
		    httpClient1.setRequestMethod("POST");
	        httpClient1.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient1.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient1.setDoOutput(true);
	        httpClient1.getResponseCode();
	        httpClient1.connect();
	        
			final String uri2 = "http://localhost:8080/api/stagingAttendance/delete";
		    HttpURLConnection httpClient2 = (HttpURLConnection) new URL(uri2).openConnection();
		    httpClient2.setRequestMethod("DELETE");
	        httpClient2.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient2.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient2.setDoOutput(true);
	        httpClient2.getResponseCode();
	        httpClient2.connect();
	        
	        System.out.println("Penarikan data absensi berjalan setiap jam 10 malam.");
			System.out.println("-----------------------------------------------");
		} catch (Exception e) {
			System.out.println("Penarikan data absensi tidak berjalan. " + e.getMessage());
			System.out.println("-----------------------------------------------");
			e.printStackTrace();
		}		
	}
}
