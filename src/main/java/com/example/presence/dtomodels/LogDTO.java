package com.example.presence.dtomodels;

public class LogDTO {
	private Long logId;
	private String errorMessage;

	public LogDTO() {
		// TODO Auto-generated constructor stub
	}

	public LogDTO(Long logId, String errorMessage) {
		super();
		this.logId = logId;
		this.errorMessage = errorMessage;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
