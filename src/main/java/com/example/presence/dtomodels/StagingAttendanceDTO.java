package com.example.presence.dtomodels;

public class StagingAttendanceDTO {
	private Long id;
	private Long pin;
	private String dateTime;
	private String verified;
	private String status;
	private String workCode;

	public StagingAttendanceDTO() {
		// TODO Auto-generated constructor stub
	}

	public StagingAttendanceDTO(Long id, Long pin, String dateTime, String verified, String status, String workCode) {
		super();
		this.id = id;
		this.pin = pin;
		this.dateTime = dateTime;
		this.verified = verified;
		this.status = status;
		this.workCode = workCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPin() {
		return pin;
	}

	public void setPin(Long pin) {
		this.pin = pin;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWorkCode() {
		return workCode;
	}

	public void setWorkCode(String workCode) {
		this.workCode = workCode;
	}
	
}
