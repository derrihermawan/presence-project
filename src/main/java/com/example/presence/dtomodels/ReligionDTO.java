package com.example.presence.dtomodels;

public class ReligionDTO {
	private Long religionId;
	private String religionName;
	
	public ReligionDTO() {
		// TODO Auto-generated constructor stub
	}

	public ReligionDTO(Long religionId, String religionName) {
		super();
		this.religionId = religionId;
		this.religionName = religionName;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public String getReligionName() {
		return religionName;
	}

	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}
	
}
