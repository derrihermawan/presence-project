package com.example.presence.dtomodels;

public class StatusDTO {
	private Long statusId;
	private String statusName;
	
	public StatusDTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
}
