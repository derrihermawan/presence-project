package com.example.presence.dtomodels;

import java.util.Date;

public class AttendanceDTO {
	private Long attendanceId;
	private EmployeeDTO employee;
	private Date date;
	private Date timeIn;
	private Date timeOut;
	private Date lateTimeDuration;
	private Date earlyInDuration;
	private Date workingTimeDuration;
	
	public AttendanceDTO() {
		// TODO Auto-generated constructor stub
	}

	public AttendanceDTO(Long attendanceId, EmployeeDTO employee, Date date, Date timeIn, Date timeOut,
			Date lateTimeDuration, Date earlyInDuration, Date workingTimeDuration) {
		super();
		this.attendanceId = attendanceId;
		this.employee = employee;
		this.date = date;
		this.timeIn = timeIn;
		this.timeOut = timeOut;
		this.lateTimeDuration = lateTimeDuration;
		this.earlyInDuration = earlyInDuration;
		this.workingTimeDuration = workingTimeDuration;
	}

	public Long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public EmployeeDTO getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeDTO employee) {
		this.employee = employee;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}

	public Date getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Date timeOut) {
		this.timeOut = timeOut;
	}

	public Date getLateTimeDuration() {
		return lateTimeDuration;
	}

	public void setLateTimeDuration(Date lateTimeDuration) {
		this.lateTimeDuration = lateTimeDuration;
	}

	public Date getEarlyInDuration() {
		return earlyInDuration;
	}

	public void setEarlyInDuration(Date earlyInDuration) {
		this.earlyInDuration = earlyInDuration;
	}

	public Date getWorkingTimeDuration() {
		return workingTimeDuration;
	}

	public void setWorkingTimeDuration(Date workingTimeDuration) {
		this.workingTimeDuration = workingTimeDuration;
	}
	
}
