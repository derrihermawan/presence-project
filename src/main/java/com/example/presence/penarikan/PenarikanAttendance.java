package com.example.presence.penarikan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.json.JSONArray;

import com.example.presence.config.Konfigurasi;
import com.example.presence.helperpenarikan.Parser;


public class PenarikanAttendance {
	
	public String getDataFromMachine() throws Exception{			
		Socket socket = null;
		Parser parser = new Parser();	
		Konfigurasi konfigurasi = new Konfigurasi();
		JSONArray result = konfigurasi.konfigur();
		String host = result.getJSONObject(0).getString("host_socket");
		int port = result.getJSONObject(0).getInt("port_socket");
		
		
		String cleanedData ="";
		try {  
		    String data = "<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">"+"0"+"</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
		    socket = new Socket(host, port);
		    String path = "/iWsService";
		    BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
		    String newLine = "\r\n";
		    wr.write("POST " + path + " HTTP/1.0" + newLine);
		    wr.write("Content-Type: text/xml" + newLine);	
		    wr.write("Content-Length: " + data.length() + newLine + newLine);	
		    wr.write(data + newLine);
		    wr.write(data);
		    wr.flush();
		    
		    BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		    System.out.println();
		    String line = rd.readLine();
		    String buffer = "";
		    do {
		    	buffer += parser.cleanAttLine(line, "<Row>","</Row>");		    	
		    } while ((line = rd.readLine()) != null);
		    
		    // remove header and footer
		    cleanedData = parser.cleanHeader(buffer, "<GetAttLogResponse>", "</GetAttLogResponse>");
		 
		    
		    
		}
		catch(IOException e) {
			System.out.println("Raspberry: Gagal koneksi ke mesin fingerprint");
		}
		socket.close();
		return cleanedData;
	}
}
