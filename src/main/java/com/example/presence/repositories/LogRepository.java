package com.example.presence.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.presence.models.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {
	//insert into attendance
	@Modifying
	@Query(value = "INSERT INTO log (error_message) values(:errorMessage)", 
			nativeQuery = true)	
	@Transactional
	void insertLog(@Param("errorMessage") String errorMessage);
	
}
