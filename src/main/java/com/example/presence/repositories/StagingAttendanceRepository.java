package com.example.presence.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.presence.models.StagingAttendance;

@Repository
public interface StagingAttendanceRepository extends JpaRepository<StagingAttendance, Long> {
	
	//insert into staging attendance
	@Modifying
	@Query(value = "INSERT INTO staging_attendance (pin, date_time, verified, status, work_code) values(:pin, :dateTime, :verified, :status, :workCode)", 
			nativeQuery = true)	
	@Transactional
	void insert(@Param("pin") Long pin, @Param("dateTime") String dateTime, @Param("verified") String verified, @Param("status") String status, @Param("workCode") String workCode);
	
	//delete staging attendance
	@Modifying
	@Query(value = "DELETE from staging_attendance", 
			nativeQuery = true)	
	@Transactional
	void delete();
	
}
