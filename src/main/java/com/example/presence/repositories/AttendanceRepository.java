package com.example.presence.repositories;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.presence.models.Attendance;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
	//insert into attendance
	@Modifying
	@Query(value = "INSERT INTO attendance (nip, date, time_in, time_out, late_time_duration, early_in_duration, working_time_duration) values(:nip, :date, :timeIn, :timeOut, :lateTimeDuration, :earlyInDuration, :workingTimeDuration)", 
			nativeQuery = true)	
	@Transactional
	void insert(@Param("nip") Long nip, @Param("date") Date date, @Param("timeIn") Date timeIn, @Param("timeOut") Date timeOut, @Param("lateTimeDuration") Date lateTimeDuration, @Param("earlyInDuration") Date earlyInDuration, @Param("workingTimeDuration") Date workingTimeDuration);
	
	//update into attendance
	@Modifying
	@Query(value = "UPDATE attendance SET time_out =:timeOut, working_time_duration = :workingTimeDuration WHERE nip = :nip AND date = :date", 
			nativeQuery = true)	
	@Transactional
	void update(@Param("nip") Long nip, @Param("date") Date date, @Param("timeOut") Date timeOut, @Param("workingTimeDuration") Date workingTimeDuration);
	
}
