package com.example.presence.helperpenarikan;

/*
 * Class Name	: Parser.java
 * Author		: 79) Algi
 * Date Created	: 
 * Date Modified: 29 Aug 2015
 * Description	: This class implement XML content parsing
 */

public class Parser {
	
	/*
	 * Method name	: cleanUserLine and cleanAttLine
	 * Method type	: String
	 * Description	: This method implement string replacement, which is removing XML tag <Row> and </Row>
	 * Return value	: String of XML content per row	
	 */
	public String cleanUserLine(String data, String p1, String p2)
	{
		String a = data.replace(p1, "");
		String b = a.replace(p2, "");
		
		String cleanFromPin = cleanField(b, "<PIN>","</PIN>");
		String cleanFromName = cleanField(cleanFromPin, "<Name>","</Name>");
		String cleanFromPassword = cleanField(cleanFromName, "<Password>","</Password>");
		String cleanFromGroup = cleanField(cleanFromPassword, "<Group>","</Group>");
		String cleanFromPrivilege = cleanField(cleanFromGroup, "<Privilege>","</Privilege>");
		String cleanFromCard = cleanField(cleanFromPrivilege, "<Card>","</Card>");
		String cleanFromUserid = cleanField(cleanFromCard, "<PIN2>","</PIN2>");
		String cleanFromTz1 = cleanField(cleanFromUserid, "<TZ1>", "</TZ1>");
		String cleanFromTz2 = cleanField(cleanFromTz1, "<TZ2>", "</TZ2>");
		String cleanedLine = cleanLastField(cleanFromTz2, "<TZ3>", "</TZ3>");
		return cleanedLine;
	}
	
	public String cleanAttLine(String data, String p1, String p2)
	{
		String a = data.replace(p1, "");
		String b = a.replace(p2, "");
		
		String cleanFromPin = cleanField(b, "<PIN>","</PIN>");
		String cleanFromDatetime = cleanField(cleanFromPin, "<DateTime>","</DateTime>");
		String cleanFromVerified = cleanField(cleanFromDatetime, "<Verified>","</Verified>");
		String cleanFromStatus = cleanField(cleanFromVerified, "<Status>","</Status>");
		String cleanedLine = cleanLastField(cleanFromStatus, "<WorkCode>","</WorkCode>");
		return cleanedLine;
	}
	
	/*
	 * Method name	: cleanField
	 * Method type	: String
	 * Description	: This method implement string replacement, which is removing XML tag to get content field 
	 * 				  and give character '_' between fields to get the actual content of a field
	 * Return value	: String of XML content cleaned from XML tags	
	 */
	private String cleanField(String data, String p1, String p2)
	{
		String a = data.replace(p1, "");
		String b = a.replace(p2, "_");
		return b;
	}	
	
	/*
	 * Method name	: cleanLastField
	 * Method type	: String
	 * Description	: This method implement string replacement, which is removing XML tag of the last field 
	 * 				  to determine content per row
	 * Return value	: String of XML content cleaned from last XML field tag 	
	 */
	private String cleanLastField(String data, String p1, String p2)
	{
		String a = data.replace(p1, "");
		String b = a.replace(p2, "\n");
		return b;
	}
	
	/*
	 * Method name	: cleanHeader
	 * Method type	: String
	 * Description	: This method implement string replacement, which is removing XML content header to get
	 * 				  the actual content
	 * Return value	: String of XML content cleaned from XML content header	
	 */
	public String cleanHeader(String data, String p1, String p2)
	{
		// get header end position
		int paramFirstPosition = data.indexOf(p1);
		int headerEndPosition = paramFirstPosition + (p1.length());
		
		// get header
		String header = data.substring(0, headerEndPosition);
			
		// remove header
		String cleanedHeader =  data.replace(header, "");
		return cleanedHeader.replace(p2, "");
	}
}
