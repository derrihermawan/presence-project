package com.example.presence.trigger;

import org.json.JSONArray;
import org.json.JSONException;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.example.presence.config.Konfigurasi;
import com.example.presence.jobpenarikan.AttendancePenarikanJob;

public class AttendancePenarikanJobTrigger {

	public JobDetail getJobDetail() {
		JobKey jobKeyAttPenarikan = new JobKey("GetAttJob", "group1");
		JobDetail attPenarikanJobDetail = JobBuilder.newJob(AttendancePenarikanJob.class)
				.withIdentity(jobKeyAttPenarikan).build();
		return attPenarikanJobDetail;
	}
	 
	public Trigger getTrigger() throws JSONException {

		Konfigurasi konfigurasi = new Konfigurasi();
		JSONArray result = konfigurasi.konfigur();
		String scheduler = result.getJSONObject(0).getString("scheduler");
		
		try {
			Trigger jobGetAttTrigger = TriggerBuilder
	    			.newTrigger()
	    			.withIdentity("getAttJobTrigger", "group1")
	    			.withSchedule(
	    					CronScheduleBuilder.cronSchedule(scheduler))
	    					.build();
			return jobGetAttTrigger;
		} catch(Exception ex) {
			System.out.println(ex);
			return null;
		}
	}
}
